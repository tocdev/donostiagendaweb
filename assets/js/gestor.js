/**
 * Created by Johan on 5/6/2015.
 */

$(document).ready(function () {

    // Permite navegar con los tabs en el gestor.
    $("li a").click(function () {
        window.location.href = ($(this).attr("href"));
    });

    refreshEventsDk();
    datepciker();
    dynatable();
});

function refreshEventsDk() {
    $("#btn-actualizar").click(function () {
            $.ajax({
                url: $(this).val(),
                beforeSend: function (xhr) {
                    $("#progress").show();
                    $("#btn-actualizar").text("Actualizando...");
                },
                success: function (data) {
                    $("#progress").hide();
                    $("#btn-actualizar").html('Actualizar eventos... <i class="mdi-navigation-refresh right"></i>');
                    Materialize.toast('Actualización correcta', 6000, 'rounded');
                },
                error: function () {
                    $("#progress").hide();
                    $("#btn-actualizar").html('Actualizar eventos... <i class="mdi-navigation-refresh right"></i>');
                    Materialize.toast('Ups! intentalo de nuevo', 6000, 'rounded');
                }
            })
        }
    )
}

function datepciker() {

    $('.datepicker').pickadate({
        format: 'd mmmm, yyyy',
        selectMonths: true,
        selectYears: 1
    });
}

function dynatable() {
    $.dynatableSetup({
        features: {perPageSelect: false}
    });
    $('#data').dynatable();
}
