<?php

/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/17/2015
 * Time: 6:48 PM
 */
class Gestor_user_messages extends Gestor_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->data ['title'] = 'Gestor - Mensajes';
        $this->data['tab'] = 'mensajes';
        $this->data['menu'] = str_replace('/', '', substr(uri_string(), strrpos(uri_string(), '/')));

        $this->menu = 'user_menu_messages';
        $this->header = 'user_header';
        $this->footer = 'user_footer';
    }

    public function index()
    {
        $this->view('gestor_user_messages');
    }
}