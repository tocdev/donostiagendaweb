<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Clase para hacer pruebas unitarias del model de eventos.
 *
 * @author Johan
 *
 */
class Event_test extends DA_Controller
{
	/**
     * Inicializa los componentes necesarios.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('unit_test');
        $this->load->model('Event_model');
        
        $this->data['title'] = 'Pruebas unitarias';
    }
    
    /**
     * Ejecuta los test unitarios e imprime sus resultados.
     */
    public function index()
    {
        $this->_template();
        $this->_test_events();
        echo $this->unit->report();
    }
    
    /**
     * Establece una plantilla para ver los datos.
     */
    private function _template() 
    {
        $this->unit->set_template('
            <table border="0" cellpadding="4" cellspacing="1">
            {rows}
                    <tr>
                            <td>{item}</td>
                            <td>{result}</td>
                    </tr>
            {/rows}
            </table>
            '); 
    }
    
    /**
     * Ejecuta los test unitarios del modelo de eventos.
     */
    private function _test_events() 
    {
        $data_events = $this->Event_model->get_events();
        $this->unit->run($data_events, 'is_array', 'Obtener eventos');
        
        $data_events = $this->Event_model->get_events('2');
        $this->unit->run($data_events, 'is_array', 'Obtener eventos por id de usuario',
                    'El id de usuario pasado es el de DonostiaKultura.');
    }
}