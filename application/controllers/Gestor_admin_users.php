<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador del gestor del administrador.
 *
 * En esta clase se definen y controlan todas las operaciones que puede
 * realizar un usuario administrador.
 *
 * @author Johan
 *
 */
class Gestor_admin_users extends Gestor_Controller
{

    /**
     * Inicialziza los componentes necesarios y comprueba los privilegios
     * del usuario.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['title'] = 'Gestor - Administrador';
        $this->data['tab'] = 'usuarios';

        $this->menu = 'gestor_menu_usuarios';
        $this->header = 'gestor_header';
        $this->footer = 'gestor_footer';

        $this->data['menu'] = str_replace('/', '', substr(uri_string(), strrpos(uri_string(), '/')));

        // Comprobamos sí el usuario tiene permisos de administrador
        if ($this->type != 1) {
            // TODO: Direccionar a una página que muestre un acceso prohibido.
            redirect('inicio');
        }
    }

    /**
     * Renderiza las vistas del administrador.
     */
    public function index()
    {
        $this->users('web');
    }

    /**
     * Muestra los usuarios en una tabla según el tipo pasado.
     *
     * @param string $type
     */
    public function users($type)
    {
        $users = array();

        if ($type == 'web') {
            $data_users = $this->User_model->get_users_by_type(2);
        } else if ($type == 'mobile') {
            $data_users = $this->User_model->get_users_by_type(3);
        }


        if ($data_users !== FALSE) {
            foreach ($data_users as $user) {
                $users[] = array(
                    'id_user' => $user->id_user,
                    'username' => $user->username,
                    'name' => $user->name,
                    'email' => $user->email);
            }

            $this->data['users_data'] = $users;
        }

        $this->view('gestor_admin_users');
    }

    /**
     * Inserta los datos de un nuevo usuario web.
     */
    public function new_user_web()
    {
        $this->form_validation->set_rules('username', '', 'trim|required');
        $this->form_validation->set_rules('name', '', 'trim|required');
        $this->form_validation->set_rules('email', '', 'trim|required');
        $this->form_validation->set_rules('sitename', '', 'trim|required');
        $this->form_validation->set_rules('latitude', '', 'trim|required');
        $this->form_validation->set_rules('longitude', '', 'trim|required');
        $this->form_validation->set_rules('summary', '', 'required');


        if ($this->form_validation->run() == FALSE) {
            $this->view('gestor_admin_users_new');
        } else {
            $this->_new_user_process();
            $this->view('gestor_admin_users_new');
        }
    }

    /**
     * Sube la imágen e inserta los datos obtenidos a través del
     * formulario.
     *
     * @return bool
     */
    private function _new_user_process()
    {
        $image_data = $this->_upload_image();

        if (!$image_data) {
            return FALSE;
        }

        // TODO: Generar el password aútomáticamente.
        $user_data = array(
            'id_type' => 2,
            'username' => $this->input->post('username'),
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'password' => 'donostiagenda'
        );

        // TODO: Obtener el campo id_zone correcto
        $site_data = array(
            'id_zone' => 1,
            'name' => $this->input->post('sitename'),
            'summary' => $this->input->post('summary'),
            'image' => $image_data['file_name'],
            'latitude' => $this->input->post('latitude'),
            'longitude' => $this->input->post('longitude')
        );

        if ($this->User_model->new_user_web($user_data, $site_data)) {
            $this->data['result'] = 'Usuario insertado correctamente';
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Sube una nueva imagen.
     *
     * Devuelve los datos de la imagen o false si hay un error.
     *
     * @return mixed
     */
    private function _upload_image()
    {
        // Configuración para subir la imagen.
        $config['upload_path'] = './uploads/sites/';
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 2000;
        $config['max_width'] = 3000;
        $config['max_height'] = 3000;

        $this->load->library('upload', $config);

        // Sube la imagen enviada desde el formulario
        if ($this->upload->do_upload()) {
            return $this->upload->data();
        }

        $this->data['error'] = $this->upload->display_errors();
        return FALSE;
    }
}
