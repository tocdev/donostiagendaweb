<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador de eventos.
 *
 * En esta clase se definen y controlan todas las operaciones que se puede
 * realizar un usuario con los eventos.
 *
 * @author Johan
 *
 */
class Gestor_admin_events extends Gestor_Controller
{
    /**
     * Inicialziza los componentes necesarios y comprueba los privilegios
     * del usuario.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Event_model');
        $this->load->model('Category_model');
        $this->load->library('Importio', $this->config->item('importio'));

        $this->menu = 'gestor_menu_eventos';
        $this->header = 'gestor_header';
        $this->footer = 'gestor_footer';

        $this->data['title'] = 'Gestor - Administrador';
        $this->data['tab'] = 'eventos';
        $this->data['menu'] = str_replace('/', '', substr(uri_string(), strrpos(uri_string(), '/')));

        // Comprobamos sí el usuario tiene permisos de administrador
        if ($this->type != 1) {
            // TODO: Direccionar a una página que muestre un acceso prohibido.
            redirect('inicio');
        }
    }

    /**
     * Renderiza las vistas del administrador de eventos.
     */
    public function index()
    {
        $this->view('gestor_admin_events_dk');
    }

    /**
     * Renderiza las vistas para la actualziación de eventos..
     */
    public function refresh()
    {
        $this->view('gestor_admin_events_refresh');
    }

    /*
     * Obtiene los eventos existentes en la agenda de Donostia Kultura,
     * para posteriormente actualizar la información en la base de datos.
     */
    public function import_events()
    {
        $insert_events = array();
        $id_event = 0;

        // Cada elemento contenido en $package_events, es un paquete con una
        // lista de 10 eventos.
        $events_package = $this->importio->get_events_dk();

        foreach ($events_package as $events_list) {
            foreach ($events_list->results as $event) {
                $id_event++;
                $insert_events[] = array(
                    'id_event' => $id_event,
                    'id_category' => 1,
                    'id_site' => 1,
                    'url' => $event->url,
                    'datetime' => $event->date
                );
            }
        }

        if ($this->Event_model->insert_events_dk($insert_events)) {
            $events_url = array();
            foreach ($insert_events as $event) {
                $events_url[] = $event['url'];
            }
    
            $events_package = $this->importio->get_event_dk($events_url);
            $insert = array();
    
            foreach ($events_package as $events_list) {
                foreach ($events_list->results as $event) {
                    $insert[] = array(
                        'id_user' => 2,
                        'id_category' => $this->Category_model->get_category_id($event->category),
                        'id_site' => 1,
                        'state' => TRUE,
                        'title' => $event->title,
                        'summary' => $event->content,
                        'datetime' => $event->datetime,
                        'image' => $event->image
                    );
                }
            }
    
            if ($this->Event_model->insert_events($insert)) {
                $this->data['import_result'] = 'Actualización correcta';
            } else {
                $this->data['import_result'] = 'Ups! intentalo de nuevo';
            }
        }
        else {
            $this->data['import_result'] = 'Ups! intentalo de nuevo';
        }
    }

    /**
     * Muestra los eventos de Donostia Kultura almacenados en la base
     * de datos.
     */
    public function events_dk()
    {
        $events = array();
        $data_events = $this->Event_model->get_events_dk();

        if ($data_events !== FALSE) {
            foreach ($data_events as $event) {
                $events[] = array(
                    'id_event' => $event->id_event,
                    'url' => $event->url,
                    'datetime' => $event->datetime
                );
            }

            $this->data['events_dk'] = $events;
        }

        $this->view('gestor_admin_events_dk');
    }

    /**
    * Muestra los eventos que han sido insertados por los usuarios.
    */
    public function events_users()
    {
        $events = array();
        $data_events = $this->Event_model->get_events();

        if ($data_events !== FALSE) {
            foreach ($data_events as $event) {
                $events[] = array(
                    'id_user' => $event->id_user,
                    'state' => $event->state ? 'Activo' : 'Cerrado',
                    'title' => $event->title,
                    'datetime' => $event->datetime
                );
            }

            $this->data['events'] = $events;
        }

        $this->view('gestor_admin_events_users');
    }
}