<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/16/2015
 * Time: 10:48 AM
 */
defined('BASEPATH') or exit ('No direct script access allowed');

class Gestor_admin_messages extends Gestor_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->data['title'] = 'Gestor - Administrador';
        $this->data['tab'] = 'mensajes';

        $this->menu = 'gestor_menu_mensajes';
        $this->header = 'gestor_header';
        $this->footer = 'gestor_footer';

        // Comprobamos sí el usuario tiene permisos de administrador
        if ($this->type != 1) {
            // TODO: Direccionar a una página que muestre un acceso prohibido.
            redirect('inicio');
        }
    }

    public function index()
    {
        $this->view('gestor_admin_messages');
    }

}