<?php

/**
 * Clase controladora de sesión.
 *
 * Permite a los usuarios iniciar en el gestor.
 *
 * @author Johan
 *
 */
class Gestor_login extends Gestor_Controller
{

    /**
     * Gestor al que se direccionará según el tipo de usuario.
     *
     * @var string
     */
    private $gestor;

    /**
     * Inicializa los componentes necesarios para controlar las sesiones.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data ['title'] = 'Gestor';
    }

    /**
     * Visualiza el contenido de la página.
     */
    public function index()
    {
        $this->form_validation->set_rules('username', '', 'trim|required');
        $this->form_validation->set_rules('password', '', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->view('gestor_login');
        } elseif ($this->_login_process()) {
            redirect($this->gestor);
        } else {
            $this->view('gestor_login');
        }
    }

    /**
     * Verifica las credenciales de un usuario desde el formulario
     * hasta la base de datos para saber si estos son correctos.
     *
     * @return boolean
     */
    private function _login_process()
    {

        // Obtenemos los datos enviados en el formulario.
        $user_data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );

        // Comprobamos las credenciales en la base de datos.
        if ($this->User_model->login($user_data)) {

            // Leemos los datos del usuario y los a añadimos a la sesión.
            $result = $this->User_model->read_user_information($this->input->post('username'));
            $user_data = array(
                'id_user' => $result [0]->id_user,
                'name' => $result [0]->name,
                'username' => $result [0]->username,
                'email' => $result [0]->email,
                'type' => $result [0]->id_type,
                'logged_in' => TRUE
            );

            if ($user_data ['type'] == 1) {
                $this->gestor = 'gestor/admin/usuarios/web';
            } else {
                $this->gestor = 'gestor/eventos/lista';
            }

            $this->session->set_userdata($user_data);

            return TRUE;
        } else {
            $this->data ['error'] = 'Usuario o Contraseña incorrecta';
        }

        return FALSE;
    }


    /**
     * Cierra la sesión de usuario abierta.
     */
    public function logout()
    {
        $user_data = array(
            'name',
            'username',
            'email',
            'type',
            'logged_in'
        );
        $this->session->unset_userdata($user_data);
        redirect('gestor');
    }
}