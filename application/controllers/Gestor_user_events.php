<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador del gestor del usuario web.
 *
 * En esta clase se definen y controlan todas las operaciones que puede
 * realizar un usuario web.
 *
 * @author Johan
 */
class Gestor_user_events extends Gestor_Controller
{

    /**
     * Inicializa los componentes necesarios y comprueba los privilegios
     * del usuario.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Event_model');
        $this->load->model('Category_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');

        $this->data ['title'] = 'Gestor - Eventos';
        $this->data ['tab'] = 'eventos';
        $this->data ['menu'] = str_replace('/', '', substr(uri_string(), strrpos(uri_string(), '/')));

        $this->menu = 'user_menu_events';
        $this->header = 'user_header';
        $this->footer = 'user_footer';

        // Comprobamos sí el usuario tiene permisos.
        if ($this->type != 2) {
            // TODO: Direccionar a una página que muestre un acceso prohibido.
            redirect('inicio');
        }
    }

    /**
     * Obtiene y muestra en una tabla los eventos que ha insertado
     * el usuario.
     */
    public function list_events()
    {
        $events = array();
        $data_events = $this->Event_model->get_events($this->session->userdata('id_user'));

        if ($data_events !== FALSE) {
            foreach ($data_events as $event) {
                $events[] = array(
                    'state' => $event->state ? 'Activo' : 'Cerrado',
                    'title' => $event->title,
                    'datetime' => $event->datetime
                );
            }

            $this->data['events'] = $events;
        }

        $this->view('gestor_user_events');
    }

    /**
     * Inserta los datos de un nuevo evento
     */
    public function new_event()
    {
        $this->data ['categories'] = $this->Category_model->get_categories();
        
        $this->form_validation->set_rules('title', '', 'trim|required');
        $this->form_validation->set_rules('summary', '', 'trim|required');
        $this->form_validation->set_rules('datetime', '', 'required');
        $this->form_validation->set_rules('video', '', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->view('gestor_user_events_new');
        } else {
            $this->_new_event_procces();
            $this->view('gestor_user_events_new');
        }
    }

    /**
     * Sube la imágen e inserta los datos obtenidos a través del
     * formulario.
     *
     * @return bool
     */
    private function _new_event_procces()
    {
        $image_data = $this->_upload_image();

        if (!$image_data) {
            return FALSE;
        }

        $event_data = array(
            'id_user' => $this->session->userdata('id_user'),
            'id_category' => $this->input->post('category'),
            'id_site' => 1,
            'state' => TRUE,
            'title' => $this->input->post('title'),
            'summary' => $this->input->post('summary'),
            'datetime' => $this->input->post('datetime'),
            'image' => base_url('uploads/events/') . '/' . $image_data ['file_name'],
            'video' => $this->input->post('video'),
        );

        if ($this->Event_model->new_event($event_data)) {
            $this->data['result'] = 'Evento insertado correctamente';
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Sube una nueva imagen.
     *
     * Devuelve los datos de la imagen o false si hay un error.
     *
     * @return mixed
     */
    private function _upload_image()
    {
        // Configuración para subir la imagen.
        $config['upload_path'] = './uploads/events/';
        $config['allowed_types'] = 'jpg|png';
        $config['encrypt_name'] = TRUE;
        $config['max_size'] = 2000;
        $config['max_width'] = 3000;
        $config['max_height'] = 3000;

        $this->load->library('upload', $config);

        // Sube la imagen enviada desde el formulario
        if ($this->upload->do_upload()) {
            return $this->upload->data();
        }

        $this->data['error'] = $this->upload->display_errors();
        return FALSE;
    }
}