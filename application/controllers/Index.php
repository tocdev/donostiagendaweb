<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/16/2015
 * Time: 10:40 AM
 */

defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador de la página de Inicio.
 *
 * @author Johan
 *
 */
class Index extends DA_Controller
{

    /**
     * Inicializa los componentes necesarios para el inicio.
     */
    public function __construct()
    {
        parent::__construct();

        $this->data['title'] = 'Inicio';
    }

    /**
     * Visualiza el contenido de la página.
     */
    public function index()
    {
        $this->view('home');
    }
}
