<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/17/2015
 * Time: 11:17 PM
 */

defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Class Events_api
 *
 * API RESTful para obtener información de los eventos de DonostiAgenda
 *
 * @author Johan
 */
class Events_api extends REST_Controller
{
    /**
     * Iniciliza los componentes necesarios, incluido el modelo.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Event_model');
    }

    /**
     * Devuelve los eventos existentes.
     *
     * Si no se especifica la categoria devolverá todos los eventos.
     */
    public function events_get()
    {
        if ($this->get('id_category')) {
            $events = $this->Event_model->get_events_by_category($this->get('id_category'));
        } else {
            $events = $this->Event_model->get_events();
        }

        if ($events) {
            $this->response($events, 200);
        } else {
            $this->response(array('error' => 'No hay eventos!'), 404);
        }
    }
}