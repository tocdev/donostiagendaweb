<?php

/**
 * Modelo de usuarios.
 *
 * Provee todas las operaciones necesarias para controlar las sesiones
 * de los usuarios.
 *
 * @author Johan
 *
 */
class User_model extends CI_Model
{

    /**
     * Inicializa los componentes necesarios.
     */
    public function __construct()
    {

        // Carga e inicializa la clase database.
        // Esta clase nos permite interactuar con la base de datos.
        $this->load->database();
    }

    /**
     * Comprueba las credenciales de un usuario en la base de datos.
     *
     * @param array $user_data
     *            Credenciales.
     * @return boolean
     */
    public function login($user_data)
    {
        $this->db->select('*');
        $this->db->from('users_web');
        $this->db->where(array(
            'username' => $user_data ['username'],
            'password' => $user_data ['password']
        ));
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Obtiene la informacion de un usuario desde la base de datos.
     *
     * @param array $user_name
     * @return boolean
     */
    public function read_user_information($user_name)
    {
        $this->db->select('*');
        $this->db->from('users_web');
        $this->db->where('username', $user_name);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Devuelve todos los usuarios según el tipo indicado.
     *
     * @param string $type
     * @return bool
     */
    public function get_users_by_type($type)
    {
        $this->db->select('*');
        $this->db->from('users_web');
        $this->db->where('id_type', $type);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Inserta un nuevo usuario con el sitio de este.
     *
     * @param array $user_data
     * @param array $site_data
     * @return bool
     */
    public function new_user_web($user_data, $site_data)
    {
        // TODO: Encriptar el password antes de insertar el usuario.

        // Se debe insertar tanto el usuario como el nuevo sitio, si no,
        // la operacón será rechazada.
        $this->db->trans_begin();
        $this->db->insert('users_web', $user_data);
        $this->db->select('id_user');
        $this->db->from('users_web');
        $this->db->order_by('id_user', 'DESC');
        $this->db->limit(1);

        $id_user = $this->db->get()->result()[0]->id_user;
        $site_data['id_user'] = $id_user;
        $this->db->insert('sites', $site_data);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}

?>