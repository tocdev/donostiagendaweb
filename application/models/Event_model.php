<?php

/**
 * Modelo de eventos de DonostiAgenda.
 *
 * Provee todas las operaciones necesarias para tratar los datos de los eventos.
 *
 * @author Johan
 *
 */
class Event_model extends CI_Model
{
    /**
     * Inicializa los componentes para usar el modelo
     */
    public function __construct()
    {
        // Carga e inicializa la clase database.
        // Esta clase nos permite interactuar con al base de datos.
        $this->load->database();
    }

    /**
     * Inserta los eventos de Donostia Kultura.
     *
     * @param $events Array multidimensional que contiene la información
     * de todos los nuevos eventos.
     *
     * @return boolean
     */
    public function insert_events_dk($events)
    {
        // Eliminamos los eventos existentes en la tabla e insertamos
        // los nuevos en esta. Las operaciones se realizarán en una
        // transacción para asegurar que siempre tenemos datos en la tabla.
        $this->db->trans_begin();
        $this->db->empty_table('events_dk');
        $this->db->insert_batch('events_dk', $events);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /**
     * Inserta multiples eventos.
     *
     * @param $events Array multidimensional que contiene la información
     * de los nuevos eventos.
     *
     * @return boolean
     */
    public function insert_events($events)
    {
        // Eliminamos los eventos de DonostiaKultura existentes en la tabla e insertamos
        // los nuevos en esta. Las operaciones se realizarán en una
        // transacción para asegurar que siempre tenemos datos en la tabla.
        $this->db->trans_begin();
        $this->db->where('id_user', 2);
        $this->db->delete('events_users');
        $this->db->insert_batch('events_users', $events);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /**
     * Devuelve los eventos de Donostia Kultura.
     *
     * @return boolean
     */
    public function get_events_dk()
    {
        $query = $this->db->get('events_dk');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Devuelve los eventos de los usuarios.
     *
     * Si se especifica un id de usuario, se devolerán solo los eventos
     * del usuario especificado.
     *
     * @param int $id_user
     * @return bool
     */
    public function get_events($id_user = null)
    {
        if (isset($id_user)) {
            $this->db->select('*');
            $this->db->from('events_users');
            $this->db->where('id_user', $id_user);
            $query = $this->db->get();
        } else {
            $query = $this->db->get('events_users');
        }

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
    
    /**
     * Devuelve los eventos filtrados por categoria.
     *
     * @param int $id_user
     * @return bool
     */
    public function get_events_by_category($id_category)
    {
        $this->db->select('*');
        $this->db->from('events_users');
        $this->db->where('id_category', $id_category);
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

    /**
     * Inserta un nuevo evento de usuario en la base de datos.
     *
     * @param array $event
     * @return bool
     */
    public function new_event($event)
    {
        return $this->db->insert('events_users', $event);
    }
}