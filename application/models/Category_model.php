<?php

/**
 * Modelo de categorias de DonostiAgenda.
 *
 * Provee todas las operaciones necesarias para obtener datos de las
 * categorias de los eventos.
 *
 * @author Johan
 *
 */
class Category_model extends CI_Model
{

   /**
	* Inicialización de los componentes.
	*/
	public function __construct()
	{
		$this->load->database();
	}
	
   /**
	* Devuelve las categorias.
	*
	* @return mixed
	*/
	public function get_categories() 
	{
		$query = $this->db->get('categories');
		if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
	}
	
   /**
	* Devuelve el id de una categoria especificada.
	*
	* @param string $name
	* @return int
	*/
	public function get_category_id($name) 
	{
		$this->db->like('name', $name);
		$query = $this->db->get('categories');
		
		if ($query->num_rows() > 0) {
            return $query->result()[0]->id_category;
        } else {
			// Si no encuentra ninguna categoria, lo insertamos en otros
            return 8;
        }
	}
}