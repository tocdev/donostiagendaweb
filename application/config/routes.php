<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/*
 * | -------------------------------------------------------------------------
 * | URI ROUTING
 * | -------------------------------------------------------------------------
 * | This file lets you re-map URI requests to specific controller functions.
 * |
 * | Typically there is a one-to-one relationship between a URL string
 * | and its corresponding controller class/method. The segments in a
 * | URL normally follow this pattern:
 * |
 * | example.com/class/method/id/
 * |
 * | In some instances, however, you may want to remap this relationship
 * | so that a different class/function is called than the one
 * | corresponding to the URL.
 * |
 * | Please see the user guide for complete details:
 * |
 * | http://codeigniter.com/user_guide/general/routing.html
 * |
 * | -------------------------------------------------------------------------
 * | RESERVED ROUTES
 * | -------------------------------------------------------------------------
 * |
 * | There are three reserved routes:
 * |
 * | $route['default_controller'] = 'welcome';
 * |
 * | This route indicates which controller class should be loaded if the
 * | URI contains no data. In the above example, the "welcome" class
 * | would be loaded.
 * |
 * | $route['404_override'] = 'errors/page_missing';
 * |
 * | This route will tell the Router which controller/method to use if those
 * | provided in the URL cannot be matched to a valid route.
 * |
 * | $route['translate_uri_dashes'] = FALSE;
 * |
 * | This is not exactly a route, but allows you to automatically route
 * | controller and method names that contain dashes. '-' isn't a valid
 * | class or method name character, so it requires translation.
 * | When you set this option to TRUE, it will replace ALL dashes in the
 * | controller and method URI segments.
 * |
 * | Examples: my-controller/index -> my_controller/index
 * | my-controller/my-method -> my_controller/my_method
 */
$route ['default_controller'] = 'index';
$route ['inicio'] = 'index';
$route ['test'] = 'event_test';

$route ['gestor'] = 'gestor_login';
$route ['gestor/logout'] = 'gestor_login/logout';

$route ['gestor/eventos/lista'] = 'gestor_user_events/list_events';
$route ['gestor/eventos/nuevo'] = 'gestor_user_events/new_event';
$route ['gestor/mensajes'] = 'gestor_user_messages';

$route ['gestor/admin/usuarios/web'] = 'gestor_admin_users/users/web';
$route ['gestor/admin/usuarios/movil'] = 'gestor_admin_users/users/mobile';
$route ['gestor/admin/usuarios/nuevo'] = 'gestor_admin_users/new_user_web';

$route ['gestor/admin/eventos/actualizar'] = 'gestor_admin_events/refresh';
$route ['gestor/admin/eventos/importar'] = 'gestor_admin_events/import_events';
$route ['gestor/admin/eventos/donostiakultura'] = 'gestor_admin_events/events_dk';
$route ['gestor/admin/eventos/usuarios'] = 'gestor_admin_events/events_users';

$route ['gestor/admin/mensajes'] = 'gestor_admin_messages';

$route ['404_override'] = '';
$route ['translate_uri_dashes'] = FALSE;
