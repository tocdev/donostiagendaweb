<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador Frontend de DonostiAgenda.
 *
 * Los controladores de las páginas públicas deben heredarde esta clase.
 * Contiene métodos y propiedades que permiten una fácil visualización de páginas.
 *
 * @author Johan
 *
 */
class Frontend_Controller extends DA_Controller
{

    /**
     * Constructor de la clase.
     */
    public function __construct()
    {
        parent::__construct();
    }
}

?>