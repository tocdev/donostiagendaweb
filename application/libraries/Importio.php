<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Importio.
 *
 * Esta clase nos permite trabajar con el API de Importio.
 * Para más información acerca del API de Import.io:
 * http://api.docs.import.io/
 *
 * @author Johan
 */
class Importio
{
    /**
     * Almacena las claves de importio definidas en el config;
     *
     * @var array
     */
    private $keys;

    public function __construct($keys)
    {
        $this->keys = $keys;
    }

    /**
     * Devuelve todos los eventos actualmente publicados en la agenda
     * cultural de DonostiaKultura. Estos eventos son devueltos en
     * listas de 10.
     *
     * @return array Lista de eventos en JSON.
     */
    public function get_events_dk()
    {
        $events_package = array();
        set_time_limit(300);

        // Obtenemos los 40 primeros eventos
        for ($i = 0; $i < 1; $i++) {

            $events_package[] = $this->_query($this->keys['list_events_connector'], array(
                'webpage/url' => 'http://www.donostiakultura.com/index.php?option=com_flexicontent&view=items&' .
                    'cid=4&id=14698&Itemid=21&lang=es&limitstart=' . $i * 10
            ), $this->keys['user_id'], $this->keys['api_key']);
        }
        return $events_package;
    }

    public function get_event_dk(array $events_url)
    {
        $events_package = array();
        set_time_limit(300);

        for ($i = 0; $i < count($events_url); $i++) {
            $events_package[] = $this->_query($this->keys['event_connector'], array('webpage/url' => $events_url[$i]),
                $this->keys['user_id'], $this->keys['api_key']);
        }

        return $events_package;
    }

    /**
     * Realiza una consulta al API y devuelve los resultados en JSON.
     *
     * @param $connectorGuid ID del API solicitado.
     * @param $input Array con los campos post.
     * @param $userGuid ID del usuario del API.
     * @param $apiKey Clave para usar el API.
     * @return mixed Devuele la consulta en JSON
     */
    private function _query($connectorGuid, $input, $userGuid, $apiKey)
    {
        $url = "https://query.import.io/store/connector/" . $connectorGuid . "/_query?_user=" . urlencode($userGuid) .
            "&_apikey=" . urlencode($apiKey);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "import-io-client: import.io PHP client",
            "import-io-client-version: 2.0.0"
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
            "input" => $input
        )));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}