<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador base de los gestores.
 *
 * Los controladores de los gestores deben heredar de esta clase.
 *
 * @author Johan
 *
 */
class Gestor_Controller extends DA_Controller
{

    /**
     * Representa el tipo de usuario que ha iniciado sesión.
     * 1. Administador.
     * 2. Usuario web.
     *
     * @var int
     */
    protected $type;

    /**
     * Inicializa los componentes necesarios para acceder al gestor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');

        // Comprobamos si el usuario ha iniciado sesion.
        if ($this->session->userdata('logged_in')) {

            $this->type = $this->session->userdata('type');

            /*
             * Si la página actual es el gestor, eviamos el usuario
             * a la página correspondiente. Así, no tendrá que introducir
             * sus credenciales cada vez que desee entrar al gestor.
             */
            if (uri_string() == 'gestor') {
                if ($this->type == 1) {
                    redirect('gestor/admin/usuarios/web');
                } elseif ($this->type == 2) {
                    redirect('gestor/eventos/lista');
                } else {
                    // TODO: Direccionar a una página que muestre un acceso prohibido.
                    redirect('inicio');
                }
            }
        } elseif (!uri_string() == 'gestor') {
            // Si no ha iniciado sesión, enviamos al usuario a la página de inicio de sesión.
            redirect('inicio');
        }
    }
}