<?php
defined('BASEPATH') or exit ('No direct script access allowed');

/**
 * Controlador base de páginas DonostiAgenda.
 *
 * Los controladores de DonostiAgenda deben heredar de esta clase para
 * aprovechar las propiedas y métodos comúnes a todas las páginas
 * definidos en esta clase.
 *
 * @author Johan
 *
 */
class DA_Controller extends CI_Controller
{

    /**
     * Header de la página.
     *
     * @var string
     */
    protected $header = 'header';

    /**
     * Footer de la página.
     *
     * @var string
     */
    protected $footer = 'footer';

    /**
     * Menú de la página.
     *
     * @var string
     */
    protected $menu = 'empty';

    /**
     * Ruta de las páginas a visualizar.
     *
     * @var string
     */
    protected $route = 'pages';

    /**
     * Datos que se pasarán a las páginas y podrán ser usados en las vistas.
     *
     * @var array
     */
    protected $data = array();

    /**
     * Inicializa los componentes que serán usados en todas las páginas.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('url');
    }

    /**
     * Visualiza la página solicitada.
     *
     * @param string $page
     *            Página a mostrar.
     */
    public function view($page)
    {
        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            // Mostramos la página de error 404.
            show_404();
        }

        if (!isset ($this->data ['title'])) {
            $this->data ['title'] = ucfirst($page);
        }

        $this->load->view('templates/' . $this->header, $this->data);
        $this->load->view('templates/' . $this->menu, $this->data);
        $this->load->view($this->route . '/' . $page, $this->data);
        $this->load->view('templates/' . $this->footer, $this->data);
    }
}

?>