<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title><?php echo $title ?></title>

    <!-- CSS  -->
    <link href="<?php echo base_url(); ?>assets/css/materialize.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url(); ?>assets/css/style.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="z-depth-1 white" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="<?php echo site_url('inicio'); ?>"
           class="brand-logo">DonostiAgenda</a>
        <ul class="right">
            <li><a href="<?php echo site_url('gestor'); ?>">Gestor</a></li>
        </ul>
        <ul class="right">
            <li><a href="#">Contacto</a></li>
        </ul>
        <ul class="right">
            <li><a href="#">Acerca</a></li>
        </ul>
        <ul class="right">
            <li><a href="<?php echo site_url('inicio'); ?>">Inicio</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i
                class="mdi-navigation-menu"></i></a>
    </div>
</nav>