<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/11/2015
 * Time: 6:38 PM
 */
?>
<main>

    <div class="row">

        <div class="col s3">
            <div class="card-panel z-depth-1 teal lighten-5">
                <div class="menu-header">Eventos</div>

                <div class="menu-items">

                    <p>
                        <a <?php if ($menu == 'donostiakultura') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/eventos/donostiakultura') ?>">
                            Eventos
                            Donostia
                            Kultura</a>
                    </p>

                    <p>
                        <a <?php if ($menu == 'usuarios') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/eventos/usuarios') ?>">Eventos Usuarios
                            Web</a>
                    </p>

                    <p>
                        <a <?php if ($menu == 'actualizar') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/eventos/actualizar') ?>/">Actualizar </a>
                    </p>
                </div>
            </div>

        </div>