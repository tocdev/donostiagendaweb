<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title><?php echo $title ?></title>

    <!-- CSS  -->
    <link href="<?php echo base_url(); ?>assets/css/materialize.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url(); ?>assets/css/style.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url(); ?>assets/css/gestor.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="<?php echo base_url(); ?>assets/css/jquery.dynatable.css"
          type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<header>
    <nav class="top-nav white">
        <div class="nav-wrapper container ">
            <a id="logo-container" href="<?php echo site_url('inicio'); ?>"
               class="brand-logo">DonostiAgenda</a>

            <ul class="right">
                <li><a href="<?php echo site_url('gestor/logout'); ?>">Salir</a></li>
            </ul>
        </div>
    </nav>

    <div class="row z-depth-1 teal">
        <div class="container">
            <div class="col s12">
                <ul class="tabs teal">
                    <li class="tab col s3 ">
                        <a <?php if ($tab == 'usuarios') {
                            echo 'class = "active"';
                        } ?> href="<?php echo site_url('gestor/admin/usuarios/web') ?>">Usuarios</a>
                    </li>
                    <li class="tab cols3 ">
                        <a <?php if ($tab == 'eventos') {
                            echo 'class = "active"';
                        } ?> href="<?php echo site_url('gestor/admin/eventos/donostiakultura') ?>">Eventos</a>
                    </li>
                    <li class="tab col s3">
                        <a <?php if ($tab == 'mensajes') {
                            echo 'class = "active"';
                        } ?> href="<?php echo site_url('gestor/admin/mensajes') ?>">Mensajes</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>
