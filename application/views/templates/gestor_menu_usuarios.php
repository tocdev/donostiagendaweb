<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/11/2015
 * Time: 6:39 PM
 */
?>
<main>

    <div class="row">

        <div class="col s3">
            <div class="card-panel z-depth-1 teal lighten-5">
                <div class="menu-header">Usuarios</div>

                <div class="menu-items">
                    <p>
                        <a <?php if ($menu == 'web') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/usuarios/web') ?>/">Usuarios Web </a>
                    </p>

                    <p>
                        <a <?php if ($menu == 'movil') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/usuarios/movil') ?>">
                            Usuarios Móvil</a>
                    </p>

                    <p>
                        <a <?php if ($menu == 'nuevo') {
                            echo 'class = "menu active a"';
                        } ?> href="<?php echo site_url('gestor/admin/usuarios/nuevo') ?>">
                            Nuevo Usuario</a>
                    </p>
                </div>
            </div>

        </div>