<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/13/2015
 * Time: 7:30 AM
 */
?>
<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">
            <div class="center">
                <div class="col s8 offset-s2">
                    <?php
                    if (isset ($result)) {
                        ?>

                        <div class="card-panel green lighten-5">
							<span class="green-text text-darken-2"><?php
                                echo $result;
                                ?>
							</span>
                        </div>
                    <?php
                    }
                    ?>
                    <?php
                    if (isset ($error)) {
                        ?>

                        <div class="card-panel red lighten-5">
							<span class="red-text text-darken-2"><?php
                                echo $error;
                                ?>
							</span>
                        </div>
                    <?php
                    }
                    ?>
                    <?php echo form_open_multipart('gestor/admin/usuarios/nuevo'); ?>
                    <h5 class="teal-text">Datos del usuario</h5>
                    <br/>

                    <div class="row">

                        <div class="input-field col s6">
                            <input id="name" name="name" type="text"
                                   class="validate" value="" autofocus required> <label
                                for="name">Nombre</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="username" name="username" type="text"
                                   class="validate" value="" required> <label
                                for="username">Nombre de usuario</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <input id="email" name="email" type="email"
                                   class="validate" value="" required> <label for="email">Email</label>
                        </div>
                    </div>

                    <h5 class="teal-text">Datos del sitio</h5>
                    <br/>

                    <div class="row">
                        <div class="input-field col s12">
                            <input id="sitename" name="sitename" type="text"
                                   class="validate" value="" required> <label
                                for="sitename">Nombre del sitio</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="latitude" name="latitude" type="number"
                                   class="validate" value="" step="any" required> <label
                                for="latitude">Latitud</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="longitude" name="longitude" type="number"
                                   class="validate" value="" step="any" required> <label
                                for="longitude">Longiud</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="summary" name="summary"
                                      class="materialize-textarea" value="" required></textarea> <label
                                for="summary">Descripcion del sitio</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="file-field input-field">
                            <input class="file-path validate col s8 offset-s4" type="text" required/>

                            <div class="btn">
                                <span>Imagen</span>
                                <input type="file" name="userfile" required/>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button class="btn waves-effect waves-light" type="submit"
                                name="action">
                            Insertar <i class="mdi-content-send right"></i>
                        </button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
</main>