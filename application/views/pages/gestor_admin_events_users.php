<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">

            <div class="center">
                <?php
                if (isset($events)) {
                    ?>
                    <table id="data" class="striped responsive-table">
                        <thead>
                        <tr>
                            <th>usuario</th>
                            <th>Estado</th>
                            <th>Titulo</th>
                            <th>Fecha</th>
                            <th></th>
                        </tr>
                        </thead>
                        <?php
                        foreach ($events as $event) {
                            echo "<tr>";
                            echo "<td>" . $event ['id_user'] . "</td>";
                            echo "<td>" . $event ['state'] . "</td>";
                            echo "<td>" . $event ['title'] . "</td>";
                            echo "<td>" . $event ['datetime'] . "</td>";
                            echo '<td> <button class="btn teal waves-effect waves-light" type="submit"
                                    name="action">
                                        <i class="mdi-editor-mode-edit center"></i> </button> </td>';
                            echo "</tr>";
                        }
                        ?>
                    </table>
                <?php
                } else {
                    echo 'No hay eventos';
                } ?>
            </div>
        </div>
    </div>
</div>
</div>
</main>
