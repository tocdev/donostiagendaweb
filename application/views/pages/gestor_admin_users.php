<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">
            <div class="center">
                <?php
                if (isset($users_data)) {
                    ?>
                    <table id="data" class="striped responsive-table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>username</th>
                            <th>name</th>
                            <th>email</th>
                        </tr>
                        </thead>
                        <?php
                        foreach ($users_data as $user) {
                            echo "<tr>";
                            echo "<td>" . $user ['id_user'] . "</td>";
                            echo "<td>" . $user ['username'] . "</td>";
                            echo "<td>" . $user ['name'] . "</td>";
                            echo "<td>" . $user ['email'] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                <?php
                } else {
                    echo 'No hay usuarios';
                } ?>
            </div>
        </div>
    </div>
</div>

</div>
</main>