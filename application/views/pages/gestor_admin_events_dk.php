<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">

            <div class="center">
                <?php
                if (isset($events_dk)) {
                    ?>
                    <table id="data" class="striped responsive-table">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>url</th>
                            <th>fecha</th>
                        </tr>
                        </thead>
                        <?php
                        foreach ($events_dk as $event) {
                            echo "<tr>";
                            echo "<td>" . $event ['id_event'] . "</td>";
                            echo "<td><a target=" . '_blank' . " href=" . $event ['url'] . ">" . $event ['url'] . "</a></td>";
                            echo "<td>" . $event ['datetime'] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                <?php
                } else {
                    echo 'No hay eventos';
                } ?>
            </div>
        </div>
    </div>
</div>
</div>
</main>
