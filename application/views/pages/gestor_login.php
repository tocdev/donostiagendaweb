<br/>
<div class="container gestor">
    <div class="row">
        <div class="col s6 offset-s3">
            <div class="card z-depth-2 center ">
                <div class="contact-section">
                    <br/> <span class="card-title teal-text">Introduce tus
						credenciales</span>

                    <div class="col s8 offset-s2">
                        <?php echo form_open('gestor'); ?>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="username" name="username" type="text"
                                       class="validate" value="" autofocus required> <label
                                    for="username">Nombre de usuario</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="password" name="password" type="password"
                                       class="validate" value="" required> <label for="password">Contraseña</label>
                            </div>
                        </div>
                        <div class="card-action">
                            <button class="btn waves-effect waves-light" type="submit"
                                    name="action">
                                Iniciar <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                        <?php echo form_close(); ?>
                        <?php
                        if (isset ($error)) {
                            ?>

                            <div class="card-panel red lighten-5">
							<span class="red-text text-darken-2"><?php
                                echo $error;
                                ?>
							</span>
                            </div>
                        <?php
                        }
                        ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>