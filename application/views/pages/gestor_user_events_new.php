<?php
/**
 * Created by PhpStorm.
 * User: Johan
 * Date: 5/16/2015
 * Time: 12:46 PM
 */
?>
<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">
            <div class="center">
                <div class="col s8 offset-s2">
                    <?php
                    if (isset ($result)) {
                        ?>

                        <div class="card-panel green lighten-5">
							<span class="green-text text-darken-2"><?php
                                echo $result;
                                ?>
							</span>
                        </div>
                    <?php
                    }
                    ?>
                    <?php
                    if (isset ($error)) {
                        ?>

                        <div class="card-panel red lighten-5">
							<span class="red-text text-darken-2"><?php
                                echo $error;
                                ?>
							</span>
                        </div>
                    <?php
                    }
                    ?>
                    <?php echo form_open_multipart('gestor/eventos/nuevo'); ?>
                    <h5 class="teal-text">Datos del evento</h5>
                    <br/>

                    <div class="row">

                        <div class="input-field col s12">
                            <input id="title" name="title" type="text"
                                   class="validate" value="" autofocus required> <label
                                for="name">Título</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="summary" name="summary"
                                      class="materialize-textarea" value="" required></textarea> <label
                                for="summary">Descripcion del evento</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s6">
                            <input id="datetime" name="datetime" type="date" class="datepicker">
                            <label for="datetime">Fecha del evento</label>
                        </div>
                        <div class="input-field col s6">
                            <select name="category">
                                <option value="" disabled selected>Elige la categoria</option>
                                <?php
                                    foreach ($categories as $category) {
                                        echo '<option value="' . $category->id_category .'">' . $category->name  . '</option>';
                                    }
                                ?>
                            </select>            
                      </div>
                    </div>
                    <div class="row">
                        <div class="file-field input-field">
                            <input class="file-path validate col s8 offset-s4" type="text" required/>

                            <div class="btn">
                                <span>Imagen</span>
                                <input type="file" name="userfile" required/>
                            </div>
                        </div>
                    </div>
                    <div class="input-field col s12">
                        <input id="video" name="video" type="text"
                               class="validate" value="" autofocus required> <label
                            for="video">URL Video</label>
                    </div>
                    <div class="card-action">
                        <button class="btn waves-effect waves-light" type="submit"
                                name="action">
                            Enviar datos<i class="mdi-content-send right"></i>
                        </button>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
</main>