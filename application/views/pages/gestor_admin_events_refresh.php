<div class="col s8">
    <div class="card-panel z-depth-1">
        <div class="row">

            <div class="center">
                <h4>Actualización de eventos</h4>

                <p>Actualiza los eventos de Donostia Kultura existentes en la base de datos</p>

                <button id="btn-actualizar" value="<?php echo site_url('gestor/admin/eventos/importar') ?>"
                        class="btn waves-effect waves-light" type="button" name="action">
                    Actualizar
                    eventos...
                    <i class="mdi-navigation-refresh right"></i>
                </button>

                <br>
                <br>

                <div class="row">
                    <div class="div col s8 offset-s2">
                        <div id="progress" class="progress" style="display: none;">
                            <div class="indeterminate"></div>
                        </div>
                    </div>
                </div>

                <?php
                if (isset($this->data['events_dk'])) {
                    echo $this->table->generate($this->data['events_dk']);
                } ?>
            </div>
        </div>
    </div>
</div>
</div>
</main>
